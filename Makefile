#!/usr/bin/make -f

texfiles=$(wildcard *.tex)
pdffiles=$(patsubst %.tex,%.pdf,$(texfiles))
idxfiles=$(patsubst %.tex,%.idx,$(texfiles))
dvifiles=$(patsubst %.tex,%.dvi,$(texfiles))
psfiles=$(patsubst %.dvi,%.ps,$(dvifiles))

all: $(dvifiles) $(psfiles) $(pdffiles)

pdf: $(pdffiles)
%.pdf: %.tex
	latex $<
	if [ -f $(patsubst %.tex,%.idx,$<) ] ; then \
		makeindex $(patsubst %.tex,%.idx,$<) ; \
	fi
	pdflatex -interaction=nonstopmode $<

dvi: $(dvifiles)
%.dvi: %.tex
	latex $<
	if [ -f $(patsubst %.tex,%.idx,$<) ] ; then \
		makeindex $(patsubst %.tex,%.idx,$<) ; \
	fi
	latex $<

ps: $(psfiles)
%.ps: %.dvi
	dvips $<

zip: AdventurePizza-TheDome.zip
AdventurePizza-TheDome.zip: $(pdffiles)
	zip $@ $<

clean:
	-rm *.log *.aux *.toc *.ps *.pdf *.dvi *.out
	-rm *.ind *.idx *.ilg
	-rm *.zip

really-clean:
	git clean -Xdf

.PHONY: all clean dvi pdf ps really-clean zip
